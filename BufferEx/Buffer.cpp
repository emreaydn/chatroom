#include "Buffer.h"

// Initialize
Buffer::Buffer( size_t size )
{
	this->mWriteIntIndex = 0;
	this->mWriteShortIndex = 0;
	this->mWriteStringIndex = 0;

	this->mReadIntIndex = 0;
	this->mReadShortIndex = 0;
	this->mReadStringIndex = 0;

	this->mIntBuffer.resize( size );
	this->mShortBuffer.resize( size );
	this->mStringBuffer.resize( size );
}

// ---------------------- Write ---------------------- //

// Write Integer
void Buffer::WriteInt32LE( size_t index, int value )
{
	index *= 4;
	mIntBuffer[ index ] = value && 0xFF;
	mIntBuffer[ index + 1 ] = value >>= 8;
	mIntBuffer[ index + 2 ] = value >>= 8;
	mIntBuffer[ index + 3 ] = value >>= 8;


	//if( index >= mWriteIndex )
	//	mWriteIndex += 4;			// NOT SURE IF WE NEED TO INCREMENT mWriteIndex
}
void Buffer::WriteInt32LE( int value )
{
	mIntBuffer[ mWriteIntIndex ] = value;
	mIntBuffer[ mWriteIntIndex + 1 ] = value >>= 8;
	mIntBuffer[ mWriteIntIndex + 2 ] = value >>= 8;
	mIntBuffer[ mWriteIntIndex + 3 ] = value >>= 8;


	mWriteIntIndex += 4;
}

// Write Short
void Buffer::WriteShort16LE( size_t index, short value )
{
	index *= 2;
	mShortBuffer[ index ] = value;
	mShortBuffer[ index ] = value >>= 8;

}

void Buffer::WriteShort16LE( short value )
{
	mShortBuffer[ mWriteShortIndex ] = value;
	mShortBuffer[ mWriteShortIndex + 1 ] = value >>= 8;


	mWriteShortIndex += 2;
}

// TODO: Write String
void Buffer::WriteString32LE( size_t index, std::string value )
{
	index *= value.size();
}

void Buffer::WriteString32LE( std::string value )
{
	for(int index = 0; index < value.size(); index++)
	{
		char temp = value[index];
		mStringBuffer[ mWriteStringIndex + index ] = temp;
		printf( "%x\n", mStringBuffer[ mWriteStringIndex + index] );
	}
	mWriteStringIndex += value.size();
}


// ---------------------- Read ---------------------- //

// Read Integer
int Buffer::ReadInt32LE( size_t index )
{
	index *= 4;

	uint32_t value = mIntBuffer[ index ];
	value |= mIntBuffer[ index + 1 ] <<= 8;
	value |= mIntBuffer[ index + 2 ] <<= 8;
	value |= mIntBuffer[ index + 3 ] <<= 8;

	return value;
}

int Buffer::ReadInt32LE( void )
{
	uint32_t value = mIntBuffer[ mReadIntIndex ];
	value |= mIntBuffer[ mReadIntIndex + 1 ] <<= 8;
	value |= mIntBuffer[ mReadIntIndex + 2 ] <<= 8;
	value |= mIntBuffer[ mReadIntIndex + 3 ] <<= 8;

	mReadIntIndex += 4;

	return value;
}

// Read Short
short Buffer::ReadShort16LE(size_t index)
{
	index *= 2;

	uint16_t value = mShortBuffer[ index ];
	value |= mShortBuffer[ index + 1 ] <<= 8;

	return value;
}

short Buffer::ReadShort16LE(void)
{
	uint16_t value = mShortBuffer[ mReadShortIndex ];
	value |= mShortBuffer[ mReadShortIndex + 1 ] <<= 8;

	mReadShortIndex += 2;

	return value;
}


// Read String
std::string Buffer::ReadString32LE(size_t index, int length)
{
	std::string returnString;
	char temp;

	for(index; index < index + length; index++)
	{
		temp = mStringBuffer[index];
		returnString.push_back( temp );
	}

	return returnString;
}

std::string Buffer::ReadString32LE( int length )
{
	std::string returnString;
	char temp;

	for(int index = mReadStringIndex; index < mReadStringIndex + length; index++)
	{
		temp = mStringBuffer[index];
		returnString.push_back( temp );
	}

	mReadStringIndex += length;

	return returnString;
}