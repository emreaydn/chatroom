// BufferExample.cpp : Defines the entry point for the console application.
//

#include "Buffer.h"

#include <iostream>
#include <string>

int main( )
{


	Buffer buffer( 20 );
	int value;

	// Write Integer
	buffer.WriteInt32LE( 255 );
	buffer.WriteInt32LE( 256 );

	// Write Short
	buffer.WriteShort16LE( 511 );
	buffer.WriteShort16LE( 1024 );

	// Read Integer
	value = buffer.ReadInt32LE( );
	std::cout << value << std::endl;

	value = buffer.ReadInt32LE();
	std::cout << value << std::endl;

	// Read Short
	value = buffer.ReadShort16LE( );
	std::cout << value << std::endl;

	value = buffer.ReadShort16LE( );
	std::cout << value << std::endl;


	// Write String
	std::string valueString = "Emre";
	std::string readString;
	buffer.WriteString32LE( valueString );

	// Read String	

	//valueString = buffer.ReadString32LE(valueString.size());
	//std::cout << "Hello " << valueString << std::endl;

	valueString = "Aydin";
	buffer.WriteString32LE( valueString );
	
	// Read String
	//valueString = buffer.ReadString32LE(valueString.size());
	//std::cout << "Hello " << valueString << std::endl;
	readString = buffer.ReadString32LE(0, 4);
	std::cout << readString << std::endl;

	readString = buffer.ReadString32LE( 4, 5 );
	std::cout << readString << std::endl;




	std::cin >> value;




	return 0;
}

