#ifndef BUFFER_HG
#define BUFFER_HG

#include <vector>
#include <iostream>
#include <string>

class Buffer {
public:
	Buffer( size_t size );

	// Write
	void WriteInt32LE( size_t index, int value );
	void WriteInt32LE( int value );

	void WriteShort16LE( size_t index, short value );
	void WriteShort16LE( short value );

	void WriteString32LE( size_t index, std::string value );
	void WriteString32LE(std::string value );

	// Read
	int ReadInt32LE( size_t index );
	int ReadInt32LE( void );

	short ReadShort16LE( size_t index );
	short ReadShort16LE( void );

	std::string ReadString32LE( size_t index, int length );
	std::string ReadString32LE( int length );

private:
	std::vector<int> mIntBuffer;
	std::vector<short> mShortBuffer;
	std::vector<char> mStringBuffer;

	int mReadIntIndex;
	int mWriteIntIndex;

	int mReadShortIndex;
	int mWriteShortIndex;

	int mReadStringIndex;
	int mWriteStringIndex;
};



#endif